﻿using googleMapsAPI.Models.Requests;
using System.Collections.Generic;

namespace googleMapsAPI.Data
{
    public abstract class InstanceBodiesHolder
    {
        protected static DeleteRequestLocationBodyModel _deleteRequestLocationBody = new DeleteRequestLocationBodyModel
        {
            place_id = IDsData.place_id
        };

        public static PostRequestLocationBodyModel postRequestLocationBody = new PostRequestLocationBodyModel
        {
            accuracy = 50,
            name = "Frontline house",
            phone_number = "(+91) 983 893 3937",
            address = "29, side layout, cohen 09",
            types = new List<string> {  "shoe park", "shop" },
            website = "http://google.com",
            language = "French -IN",
            location = new Location
            {
                lat = -38.383494,
                lng = 33.427362
            }
        };

        public static PutRequestLocationBodyModel putRequestLocationBody = new PutRequestLocationBodyModel
        {
            place_id = IDsData.place_id,
            address = "70 Summer walk, USA",
            key = UriData.getToken()
        };
    }
}
