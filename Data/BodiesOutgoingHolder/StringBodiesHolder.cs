﻿using googleMapsAPI.Business;
using System.Net.Http;

namespace googleMapsAPI.Data
{
    public class StringBodiesHolder : InstanceBodiesHolder
    {
        public static StringContent deleteRequestLocationBodyToJSON =
            SerializeMethods.ObjectToJSON(_deleteRequestLocationBody);

        public static StringContent postRequestLocationBodyToJSON =
            SerializeMethods.ObjectToJSON(postRequestLocationBody);

        public static StringContent putRequestLocationBodyToJSON()
        {
            return SerializeMethods.ObjectToJSON(putRequestLocationBody);
        }
    }
}
