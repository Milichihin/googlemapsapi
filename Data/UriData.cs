﻿namespace googleMapsAPI.Data
{
    public static class UriData
    {
        private static readonly string _tokenURN = "?key=qaclick123";
        public static string baseURI = "https://rahulshettyacademy.com";
        public static string getPlaceURL = baseURI + "/maps/api/place/get/json" + _tokenURN;
        public static string putPlaceURL = baseURI + "/maps/api/place/update/json" + _tokenURN;
        public static string postPlaceURL = baseURI + "/maps/api/place/add/json" + _tokenURN;
        public static string deletePlaceURL = baseURI + "/maps/api/place/delete/json" + _tokenURN;

        public static string getToken()
        {
            return _tokenURN.Replace("? key = ", "");
        }
    }
}
