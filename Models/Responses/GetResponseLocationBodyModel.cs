﻿namespace googleMapsAPI.Models.Responses
{
    public class GetResponseLocationBodyModel
    {
        public Location location { get; set; }
        public string accuracy { get; set; }
        public string name { get; set; }
        public string phone_number { get; set; }
        public string address { get; set; }
        public string types { get; set; }
        public string website { get; set; }
        public string language { get; set; }
    }

    public class Location
    {
        public string latitude { get; set; }
        public string longitude { get; set; }
    }
}
