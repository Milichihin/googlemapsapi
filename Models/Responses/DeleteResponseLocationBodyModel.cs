﻿namespace googleMapsAPI.Models.Responses
{
    public class DeleteResponseLocationBodyModel
    {
        public string status { get; set; }
        public string msg { get; set; }
    }
}
