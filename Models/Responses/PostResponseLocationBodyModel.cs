﻿namespace googleMapsAPI.Models.Responses
{
    public class PostResponseLocationBodyModel
    {
        public string status { get; set; }
        public string place_id { get; set; }
        public string scope { get; set; }
        public string reference { get; set; }
        public string id { get; set; }
    }
}
