﻿namespace googleMapsAPI.Models.Requests
{
    public class DeleteRequestLocationBodyModel
    {
        public string place_id { get; set; }
    }
}
