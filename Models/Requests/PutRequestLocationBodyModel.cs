﻿namespace googleMapsAPI.Models.Requests
{
    public class PutRequestLocationBodyModel
    {
        public string place_id { get; set; }
        public string address { get; set; }
        public string key { get; set; }
    }
}
