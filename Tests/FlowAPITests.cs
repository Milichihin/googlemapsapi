﻿using Newtonsoft.Json;
using NUnit.Framework;
using googleMapsAPI.Business;
using googleMapsAPI.Data;
using googleMapsAPI.Models.Responses;
using System.Threading.Tasks;
using System.Net.Http;

namespace googleMapsAPI.Tests
{
    public class FlowAPITests
    {
        [Test, Order(1)]
        [Category("FlowAPITests")]
        public void Post_Test()
        {
            var _postResponse = CustomAPIMethods.PostContentMethod(
                UriData.postPlaceURL,
                StringBodiesHolder.postRequestLocationBodyToJSON
                );

            PostResponseLocationBodyModel getObjectBody = JsonConvert
                    .DeserializeObject<PostResponseLocationBodyModel>(_postResponse.Result);

            IDsData.place_id = getObjectBody.place_id;

            Assert.AreEqual("OK", getObjectBody.status);
        }


        [Test, Order(2)]
        [Category("FlowAPITests")]
        public async Task Get_Test()
        {
            await Task.Delay(1000);

            var _getResponse = CustomAPIMethods.GetContentMethod(
                UriData.getPlaceURL + "&place_id=" + IDsData.place_id
                );

            GetResponseLocationBodyModel getObjectBody = JsonConvert
                    .DeserializeObject<GetResponseLocationBodyModel>(_getResponse.Result);
            
            Assert.Multiple(() =>
            {
                Assert.AreEqual(
                StringBodiesHolder.postRequestLocationBody.address,
                getObjectBody.address
                );

                Assert.AreEqual(
                StringBodiesHolder.postRequestLocationBody.phone_number,
                getObjectBody.phone_number
                );
            });
        }

        [Test, Order(3)]
        [Category("FlowAPITests")]
        public async Task Put_Test()
        {
            await Task.Delay(1000);

            InstanceBodiesHolder.putRequestLocationBody.place_id = IDsData.place_id;

            StringContent putRequestLocationBodyToJSON = StringBodiesHolder.putRequestLocationBodyToJSON();

            var _putResponse = CustomAPIMethods.PutContentMethod(
                UriData.putPlaceURL, 
                putRequestLocationBodyToJSON
                );

            PutResponseLocationBodyModel putObjectBody = JsonConvert
                    .DeserializeObject<PutResponseLocationBodyModel>(_putResponse.Result);

            Assert.AreEqual(
                "Address successfully updated",
                putObjectBody.msg);
        }

        [Test, Order(4)]
        [Category("FlowAPITests")]
        public void Delete_Test()
        {
            var _deleteResponse = CustomAPIMethods.DeleteCustomMethodWithBody(
                UriData.deletePlaceURL,
                StringBodiesHolder.deleteRequestLocationBodyToJSON
                ); 
            
            var _deleteResponse1 = CustomAPIMethods.DeleteContentMethod(
                UriData.deletePlaceURL + "&place_id=" + IDsData.place_id
                );

            DeleteResponseLocationBodyModel deleteObjectBody = JsonConvert
                    .DeserializeObject<DeleteResponseLocationBodyModel>(_deleteResponse.Result);

            Assert.AreEqual(
                "OK",
                deleteObjectBody.status);
        }
    }
}
