﻿using System.Threading.Tasks;
using System.Net.Http;
using googleMapsAPI.Core;
using System;
using Newtonsoft.Json;
using System.Text;

namespace googleMapsAPI.Business
{
    public class CustomAPIMethods
    {
        public static async Task<string> PostContentMethod(string uri, HttpContent body)
        {
            var response = await ClientHolder.restClient.PostAsync(uri, body);
            var content = response.Content.ReadAsStringAsync();

            return await content;
        }

        public static async Task<string> GetContentMethod(string uri)
        {
            var response = await ClientHolder.restClient.GetAsync(uri);
            var content = response.Content.ReadAsStringAsync();

            return await content;
        }

        public static async Task<string> PutContentMethod(string uri, HttpContent body)
        {
            var response = await ClientHolder.restClient.PutAsync(uri, body);
            var content = response.Content.ReadAsStringAsync();

            return await content;
        }

        public static async Task<string> DeleteContentMethod(string uri)
        {
            var response = await ClientHolder.restClient.DeleteAsync(uri);
            var content = response.Content.ReadAsStringAsync();

            return await content;
        }

        public static async Task<string> DeleteCustomMethodWithBody<T>(string uri, T myObj)
        {
            var request = new HttpRequestMessage
            {
                Method = HttpMethod.Delete,
                RequestUri = new Uri(uri),
                Content = new StringContent(JsonConvert.SerializeObject(myObj), Encoding.UTF8, "application/json")
            };

            var response = await ClientHolder.restClient.SendAsync(request);
            var content = response.Content.ReadAsStringAsync();

            return await content;
        }
    }
}
