﻿using Newtonsoft.Json;
using System.Net.Http;

namespace googleMapsAPI.Business
{
    public static class SerializeMethods
    {
        public static StringContent ObjectToJSON<T>(T bodyObjectRequest)
        {
            return new StringContent(
                JsonConvert.SerializeObject(
                    bodyObjectRequest,
                    Formatting.Indented)
             );
        }
    }
}
